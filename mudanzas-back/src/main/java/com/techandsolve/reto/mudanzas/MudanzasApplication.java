package com.techandsolve.reto.mudanzas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MudanzasApplication {

	public static void main(String[] args) {
		SpringApplication.run(MudanzasApplication.class, args);
	}

}
