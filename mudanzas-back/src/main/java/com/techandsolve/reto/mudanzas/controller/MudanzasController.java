package com.techandsolve.reto.mudanzas.controller;

import com.techandsolve.reto.mudanzas.dto.MudanzasData;
import com.techandsolve.reto.mudanzas.dto.ViajesPorDia;
import com.techandsolve.reto.mudanzas.service.MudanzasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/mudanzas/api")
public class MudanzasController {

	@Autowired
	private MudanzasService service;

	@PostMapping("/")
	public ResponseEntity<List<ViajesPorDia>> calcularMaximoViajesPorDia(@RequestBody MudanzasData data) {
		return new ResponseEntity<>(service.calcularMaximoViajesPorDia(data), HttpStatus.OK);
	}
}
