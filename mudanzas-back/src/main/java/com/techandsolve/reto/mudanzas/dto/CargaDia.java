package com.techandsolve.reto.mudanzas.dto;

import lombok.Data;
import java.util.List;

@Data
public class CargaDia {
    private int dia;
    private int cantidadElementos;
    private int pesoMinimoPorViaje = 50;
    private List<Elemento> elementos;
}
