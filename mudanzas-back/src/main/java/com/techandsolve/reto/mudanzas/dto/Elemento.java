package com.techandsolve.reto.mudanzas.dto;

import lombok.Data;

@Data
public class Elemento {

    private  int id;
    private int peso;

    public Elemento(){}

    public Elemento(int peso, int id){
        this.peso = peso;
        this.id = id;
    }
}
