package com.techandsolve.reto.mudanzas.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MudanzasData {
    private String cedulaParticipante;
    private int diasTrabajo;
    private List<CargaDia> cargasDia;

    public MudanzasData(){
        cargasDia = new ArrayList<>();
    }
}
