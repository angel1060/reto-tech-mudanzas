package com.techandsolve.reto.mudanzas.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Viaje {
    private List<Elemento> elementos;

    public Viaje(){
        elementos = new ArrayList<>();
    }

    public Viaje addElemento(Elemento elemento){
        this.getElementos().add(elemento);
        return this;
    }

}
