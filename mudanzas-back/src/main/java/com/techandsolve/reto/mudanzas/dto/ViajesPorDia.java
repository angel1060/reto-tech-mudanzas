package com.techandsolve.reto.mudanzas.dto;

import lombok.Data;
import java.util.ArrayList;
import java.util.List;

@Data
public class ViajesPorDia {
    private int dia;
    private int cantidadViajes;
    private List<Viaje> viajes;

    public ViajesPorDia(int dia, int cantidadViajes, List<Viaje> viajes){
        this.dia = dia;
        this.cantidadViajes = cantidadViajes;
        this.viajes = viajes;
    }

    public ViajesPorDia(int dia){
        this.dia = dia;
        this.viajes = new ArrayList<>();
    }

    public int getCantidadViajes(){
        return viajes.size();
    }
}
