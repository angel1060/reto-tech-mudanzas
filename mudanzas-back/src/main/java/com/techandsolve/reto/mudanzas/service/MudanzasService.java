package com.techandsolve.reto.mudanzas.service;

import com.techandsolve.reto.mudanzas.dto.MudanzasData;
import com.techandsolve.reto.mudanzas.dto.ViajesPorDia;
import java.util.List;

public interface MudanzasService {
	List<ViajesPorDia> calcularMaximoViajesPorDia(MudanzasData data);
}
