package com.techandsolve.reto.mudanzas.service.impl;

import com.techandsolve.reto.mudanzas.dto.*;
import com.techandsolve.reto.mudanzas.service.MudanzasService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MudanzasServiceImpl implements MudanzasService {
	@Override
	public List<ViajesPorDia> calcularMaximoViajesPorDia(MudanzasData data) {
		List<ViajesPorDia> maxViajesPorDia = new ArrayList<>();

		data.getCargasDia().forEach(cargasDia -> maxViajesPorDia.add(maximizarViajesDia(cargasDia)));

		return maxViajesPorDia;
	}


	/**
	 * Calcula el máximo de viajes que se pueden realizar al día
	 */
	private ViajesPorDia maximizarViajesDia(CargaDia cargaDia){
		List<Elemento> sortedCargaDiaList = sortList(cargaDia.getElementos());
		ViajesPorDia viajesPorDia = new ViajesPorDia(cargaDia.getDia());

		while (sortedCargaDiaList.iterator().hasNext()){
			Elemento elemento = sortedCargaDiaList.iterator().next();

			if( elemento.getPeso() >= cargaDia.getPesoMinimoPorViaje()){
				viajesPorDia.getViajes().add(
						new Viaje().addElemento(elemento)
				);

			}else{

				Viaje viaje = new Viaje();
				List<Elemento> lastElements = new ArrayList<>();
				int lastIdex = sortedCargaDiaList.size();
				int multiplicador = obtenerMultiplicador(elemento.getPeso(), cargaDia.getPesoMinimoPorViaje());

				if(multiplicador-1 >= lastIdex){
					if(viajesPorDia.getViajes().isEmpty()){
						viaje.getElementos().addAll(sortedCargaDiaList);
						viajesPorDia.getViajes().add(viaje);
					}else{
						viajesPorDia.getViajes()
								.get(viajesPorDia.getViajes().size()-1)
								.getElementos().addAll(sortedCargaDiaList);
					}


				}else{
					lastElements = sortedCargaDiaList.subList(lastIdex - (multiplicador-1), lastIdex);
					viaje.getElementos().add(elemento);
					viaje.getElementos().addAll(lastElements);
					viajesPorDia.getViajes().add(viaje);
				}

				sortedCargaDiaList.removeAll(lastElements);
			}

			sortedCargaDiaList.remove(elemento);

		}
		return viajesPorDia;
	}

	/*
	* Calcula cuantas veces debe multiplicarse por si mismo el elemenot para simular peso mayor a 50
	* */
	private int obtenerMultiplicador(int peso, int pesoMinimoPorViaje){
		int multiplicador = 2;
		while(peso * multiplicador < pesoMinimoPorViaje){
			multiplicador++;
		}
		return multiplicador;
	}

	/**
	 * Ordena los datos de la lista de elmentos descendentemente
	 */
	public List<Elemento> sortList(List<Elemento> elementos){
		return elementos.stream().sorted(
				Comparator.comparing(Elemento::getPeso).reversed())
				.collect(Collectors.toList());
	}

}
