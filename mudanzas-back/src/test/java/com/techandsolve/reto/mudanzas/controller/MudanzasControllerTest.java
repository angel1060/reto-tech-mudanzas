package com.techandsolve.reto.mudanzas.controller;

import com.techandsolve.reto.mudanzas.data.MudanzasServiceData;
import com.techandsolve.reto.mudanzas.dto.ViajesPorDia;
import com.techandsolve.reto.mudanzas.service.MudanzasService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class MudanzasControllerTest {

    private MudanzasServiceData dataTest;

    @InjectMocks
    private MudanzasController mudanzasController;

    @Mock
    private MudanzasService mudanzasService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        dataTest = new MudanzasServiceData();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void calcularMaximoViajesPorDia(){
        when(mudanzasService.calcularMaximoViajesPorDia(
                dataTest.crearMudanzasData_flujo1()))
                .thenReturn(dataTest.getResponseMaxViajesPorDia());

        ResponseEntity<List<ViajesPorDia>> response = mudanzasController.calcularMaximoViajesPorDia(
                dataTest.crearMudanzasData_flujo1());

        assertNotNull(response);
    }
}