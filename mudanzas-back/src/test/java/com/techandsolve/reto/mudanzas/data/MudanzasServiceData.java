package com.techandsolve.reto.mudanzas.data;

import com.techandsolve.reto.mudanzas.dto.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Data
public class MudanzasServiceData {

    List<Elemento> elementos;
    List<ViajesPorDia> responseMaxViajesPorDia;

    public MudanzasServiceData(){
        crearElementos();
        crearViajesPorDiaResponseData();
    }

    private void crearElementos(){
        elementos = new ArrayList<>();
        List<Integer> nums = Arrays.asList(5,4,30,30,1,1,3,20,20,20,11,1,2,3,4,5,6,7,8,9,10,11,6,9,19,29,39,49,59,10,32,56,76,8,44,60,47,85,71,91);
        int index = 0;

        for (Integer num: nums) {
            elementos.add(new Elemento(num, index++));
        }
    }


    public MudanzasData crearMudanzasData_flujo1(){
        MudanzasData mudanzasData = crearMudanzasDataBase();
        mudanzasData.getCargasDia().get(0)
                .getElementos().add(new Elemento(50, 1));

        return mudanzasData;
    }



    public MudanzasData crearMudanzasData_flujo2(){
        MudanzasData mudanzasData = crearMudanzasDataBase();
        mudanzasData.getCargasDia().get(0)
                .getElementos().addAll(elementos.subList(2, 6));

        return mudanzasData;
    }


    public MudanzasData crearMudanzasData_flujo3(){
        MudanzasData mudanzasData = crearMudanzasDataBase();
        CargaDia cd = mudanzasData.getCargasDia().get(0);
        cd.getElementos().add(new Elemento(10, 1));
        cd.getElementos().add(new Elemento(1, 2));
        cd.getElementos().add(new Elemento(2, 3));
        cd.getElementos().add(new Elemento(3, 4));
        cd.getElementos().add(new Elemento(4, 5));

        return mudanzasData;
    }


    public MudanzasData crearMudanzasData_flujo4(){
        MudanzasData mudanzasData = crearMudanzasDataBase();
        mudanzasData.getCargasDia().get(0)
                .getElementos().add(new Elemento(48, 1));

        return mudanzasData;
    }


    public MudanzasData crearMudanzasData_flujo5(){
        MudanzasData mudanzasData = crearMudanzasDataBase();
        mudanzasData.getCargasDia().get(0)
                .getElementos().addAll(elementos.subList(10, 22));

        return mudanzasData;
    }


    private void crearViajesPorDiaResponseData(){
        List<Viaje> viajes = new ArrayList<>();

        Viaje viaje = new Viaje();
        viaje.getElementos().addAll(elementos.subList(2, 6));
        viajes.add(viaje);

        responseMaxViajesPorDia = new ArrayList<>();
        responseMaxViajesPorDia.add(new ViajesPorDia(1, 4, viajes));
    }


    public MudanzasData crearMudanzasDataBase(){
        CargaDia cDia = new CargaDia();
        cDia.setDia(1);
        cDia.setCantidadElementos(1);
        cDia.setElementos(new ArrayList<>());

        MudanzasData mudanzasData = new MudanzasData();
        mudanzasData.setDiasTrabajo(1);
        mudanzasData.getCargasDia().add(cDia);

        return mudanzasData;
    }
}
