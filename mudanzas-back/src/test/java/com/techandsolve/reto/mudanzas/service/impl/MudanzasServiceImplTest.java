package com.techandsolve.reto.mudanzas.service.impl;

import com.techandsolve.reto.mudanzas.data.MudanzasServiceData;
import com.techandsolve.reto.mudanzas.dto.ViajesPorDia;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


class MudanzasServiceImplTest {
    private MudanzasServiceData dataTest;

    @InjectMocks
    private MudanzasServiceImpl mudanzasService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        dataTest = new MudanzasServiceData();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void calcularMaximoViajesPorDia_flujo1_1ElementoPorViaje(){
        List<ViajesPorDia> response = mudanzasService.calcularMaximoViajesPorDia(
                dataTest.crearMudanzasData_flujo1());
        assertEquals(1, response.get(0).getViajes().size());
    }

    @Test
    void calcularMaximoViajesPorDia_flujo2_noAddMultiplicador(){
        List<ViajesPorDia> response = mudanzasService.calcularMaximoViajesPorDia(
                dataTest.crearMudanzasData_flujo2());
        assertEquals(2, response.get(0).getViajes().size());
    }

    @Test
    void calcularMaximoViajesPorDia_flujo3_addMultiplicador(){
        List<ViajesPorDia> response = mudanzasService.calcularMaximoViajesPorDia(
                dataTest.crearMudanzasData_flujo3());
        assertEquals(1, response.get(0).getViajes().size());
    }

    @Test
    void calcularMaximoViajesPorDia_flujo4_soloUnElementoPesoMenor50(){
        List<ViajesPorDia> response = mudanzasService.calcularMaximoViajesPorDia(
                dataTest.crearMudanzasData_flujo4());
        assertEquals(1, response.get(0).getViajes().size());
    }

    @Test
    void calcularMaximoViajesPorDia_flujo5_agregaElementosAViajeCompleto(){
        List<ViajesPorDia> response = mudanzasService.calcularMaximoViajesPorDia(
                dataTest.crearMudanzasData_flujo5());
        assertEquals(2, response.get(0).getViajes().size());
    }

}