import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MudanzasComponent } from './pages/mudanzas/mudanzas.component';

const routes: Routes = [
  { path: '', component: MudanzasComponent },
  { path: 'mudanzas', component: MudanzasComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
