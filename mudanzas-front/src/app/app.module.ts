import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MudanzasComponent } from './pages/mudanzas/mudanzas.component';
// import { MatFileUploadModule } from 'angular-material-fileupload';
import { MudanzasService } from './services/mudanzas.service';
import { UtilsModule } from './shared/index.default';
import { AppConfig } from './config/app.config';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { UtilsService } from './services/utils.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    MudanzasComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    UtilsModule.forRoot(),
    TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
        }
    }),

  ],
  providers: [
    MudanzasService,
    AppConfig,
    UtilsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
