import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';


@Injectable()
export class AppConfig {
  readonly URL: string = environment.apiUrl;
  readonly DATE_FORMAT: string = 'YYYY-MM-DD HH:mm:ss.SSS Z';
  readonly TEXT_PLAIN: string = 'text/plain;charset=utf-8';
  readonly FILE_PLAIN_EXTENSION: string = 'txt';
}
