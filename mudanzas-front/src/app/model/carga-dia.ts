import { Elemento } from './elemento';

export class CargaDia {
    dia: number;
    cantidadElementos: number;
    elementos: Elemento[];

    constructor(dia?: number, cantidadElementos?: number, elementos?: Elemento[]) {
        this.cantidadElementos = cantidadElementos;
        this.elementos = elementos;
        this.dia = dia;
    }
}
