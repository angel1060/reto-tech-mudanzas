export class Elemento {
    peso: number;
    id: number;

    constructor(id?: number, peso?: number) {
        this.peso = peso;
        this.id = id;
    }
}
