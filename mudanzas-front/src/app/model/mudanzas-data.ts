import { CargaDia } from './carga-dia';
import { ViajesDia } from './viajes-dia';

export class MudanzasData {
    cedulaParticipante: string;
    fechaEjecucion: Date;
    diasTrabajo: number;
    cargasDia: CargaDia[];
    viajesDiaMax: ViajesDia[];

    constructor() {
        this.cargasDia = [];
    }
}
