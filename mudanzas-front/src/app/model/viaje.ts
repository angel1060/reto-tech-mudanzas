import { Elemento } from './elemento';

export class Viaje {
    elementos: Elemento[];

    constructor(elementos?: Elemento[]) {
        this.elementos = elementos;
    }
}
