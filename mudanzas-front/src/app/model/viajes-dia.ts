import { Viaje } from './viaje';

export class ViajesDia {
    dia: number;
    cantidadViajes: number;
    viajes: Viaje[];

    constructor( dia?: number, cantidadViajes?: number, viajes?: Viaje[]) {
        this.dia = dia;
        this.cantidadViajes = cantidadViajes;
        this.viajes = viajes;
    }
}
