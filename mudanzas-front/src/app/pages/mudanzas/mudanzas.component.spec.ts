import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MudanzasComponent } from './mudanzas.component';
import { TestModule } from '../../test.module';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { AppConfig } from '../../config/app.config';
import { UtilsService } from '../../services/utils.service';
import { MudanzasService } from '../../services/mudanzas.service';
import { FakeMudanzasRepository } from './repository/fake.mudanzas.repository';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { throwError } from "rxjs";

describe('MudanzasComponent', () => {
  let component: MudanzasComponent;
  let fixture: ComponentFixture<MudanzasComponent>;
  let dataTest: MudanzasDataTest;

  beforeAll( () => {
    dataTest = new MudanzasDataTest();
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        MudanzasComponent
      ],
      imports: [TestModule, TranslateModule.forRoot()],
      providers: [
        AppConfig,
        UtilsService,
        TranslateService,
        {
          provide: MudanzasService,
          useClass: FakeMudanzasRepository
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MudanzasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Crea el componente', () => {
    expect(component).toBeTruthy();
  });

  it('Convierte texto de un archivo plano a objetos typescript', () => {
    component.convertToMudanzasData(dataTest.DATA_STRING);
    expect( component.mudanzaData.cargasDia.length ).toBeGreaterThan(0);
  });

  it(`Genera error cuando existe un día que tiene menos elementos de los configurados`, () => {
      spyOn( component.utilService, 'replaceStringValues' ).and.returnValue(dataTest.PROCESS_ERROR);
      const dataString = `${dataTest.DATA_STRING}\n5`;
      let message;

      try {
        component.convertToMudanzasData(dataString);
      } catch (error) {
        message = error.message;
      }
      expect( message ).toBe(dataTest.PROCESS_ERROR);
  });

  it(`Genera error cuando en el archivo plano existe un dato no númerico`, () => {
      const dataString = `${dataTest.DATA_STRING}\n1\na`;
      let message;

      try {
        component.convertToMudanzasData(dataString);
      } catch (error) {
        message = error.message;
      }
      expect( message ).toBe('MESSAGES.ERROR_DATA_TYPE');
  });

  it(`Calcula máximos viajes por día`, () => {
    component.calcularMaximoViajesPorDia();
    expect( component.mudanzaDataList.length ).toBeGreaterThan(0);
  });

  it('Genera excepción al calcular máximos viajes por día', () => {
      spyOn( component.service, 'calcularMaximoViajesPorDia' ).and.returnValue( throwError(dataTest.PROCESS_ERROR) );
      component.calcularMaximoViajesPorDia();
      expect( component.loading ).toBeFalsy();
  });

  it('Es llamado el método onFileChange', () => {
      spyOn( component, 'loadFileData' ).and.returnValue(  );
      const event =  { target: {files: {}},  pageSize: 2 } ;

      component.onFileChange(event);
      expect( component.loading ).toBeFalsy();
  });

  it('Genera error cuando el número de días de trabajo no están dentro del rango permitido ', () => {
      let response;
      try {
        response = component.validarDiasTrabajo(1000);
      } catch (error) {}

      expect( response).toBeFalsy();
  });

  it('Genera error cuando el número de elementos a cargar no está dentro del rango permitido ', () => {
      let response;
      try {
        response = component.validarCantidadElementos(1000);
      } catch (error) {}

      expect( response).toBeFalsy();
  });

  it('Genera error cuando el peso de un elemento no está dentro del rango permitido ', () => {
      let response;
      try {
        response = component.validarPesoElemento(1000);
      } catch (error) {}
  
      expect( response).toBeFalsy();
  });

});



export class MudanzasDataTest {
  public PROCESS_ERROR = 'process error';
  public DATA_STRING =
  `5
  4
  30
  30
  1
  1
  3
  20
  20
  20
  11
  1
  2
  3
  4
  5
  6
  7
  8
  9
  10
  11
  6
  9
  19
  29
  39
  49
  59
  10
  32
  56
  76
  8
  44
  60
  47
  85
  71
  91`;
}

