import { Component, OnInit } from '@angular/core';
import { CargaDia } from '../../model/carga-dia';
import { Elemento } from '../../model/elemento';
import { MudanzasData } from '../../model/mudanzas-data';
import { MudanzasService } from '../../services/mudanzas.service';
import { ViajesDia } from '../../model/viajes-dia';
import { UtilsService } from '../../services/utils.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-mudanzas',
  templateUrl: './mudanzas.component.html',
  styleUrls: ['./mudanzas.component.css']
})
export class MudanzasComponent implements OnInit {

  public mudanzaData: MudanzasData = new MudanzasData();
  public mudanzaDataList: MudanzasData[] = [];
  public viajesDiaResp: ViajesDia[];
  public loading = false;

  constructor(
    public service: MudanzasService,
    public utilService: UtilsService,
    private translate: TranslateService) {
  }

  ngOnInit() { }

  /**
   * Evento que se ejecuta al leer el archivo seleccionado por el usuario
   */
  onFileChange(event) {
      this.loadFileData(event.target.files[0], data => {
        try {
          this.convertToMudanzasData(data);
          console.log(this.mudanzaData);
          this.calcularMaximoViajesPorDia();
        } catch (error) {
          this.utilService.errorMessage(error.message);
        }
      });
  }

  /*
  * Calcula el máximo de viajes por día
  */
  calcularMaximoViajesPorDia() {
    this.service.calcularMaximoViajesPorDia(this.mudanzaData).subscribe (
      (data) => {
          console.log(data);
          this.mudanzaData.viajesDiaMax = data;
          this.mudanzaDataList.push(Object.assign(new MudanzasData(), this.mudanzaData));
          this.loading = false;
          this.convertToText(data);
      },
      error => {
          this.loading = false;
      }
  );
  }

  /**
   * Convierte datos del archivo a objetos de tipo mudanza
   */
  convertToMudanzasData(data: string) {
      let dia = 1;
      const strSplit = data.split("\n");
      this.mudanzaData.fechaEjecucion = new Date();

      this.mudanzaData.diasTrabajo = this.validarDiasTrabajo(strSplit.splice(0, 1));

      while ( strSplit.length > 0 ) {
        const cantidadElementos = this.validarCantidadElementos(strSplit.splice(0, 1));

        if (cantidadElementos > strSplit.length) {
          throw new Error(this.utilService.replaceStringValues(
            this.translate.instant('MESSAGES.ERROR_FILE_LENGH'), [dia, cantidadElementos, strSplit.length]));
        }
        const cargaDia = new CargaDia(dia++, cantidadElementos, this.createElements(strSplit.splice(0, cantidadElementos)));
        this.mudanzaData.cargasDia.push(cargaDia);
      }
  }

  /*
  * Crea lista de elementos
  */
  private createElements(dataElements: any[]): Elemento[] {
      const elementos = [];
      let i = 0;
      dataElements.forEach(peso => {
          elementos.push(new Elemento(i++, this.validarPesoElemento(peso)));
      });
      return elementos;
  }


  /**
   * Lee los datos del archivo adjunto
   */
  loadFileData(file: File, callback) {
    const fileReader = new FileReader();
    let inputText;

    fileReader.onload = (e) => {
      inputText = String(fileReader.result).trim();
      callback(inputText);
    };
    fileReader.readAsText(file);
  }




  convertToText(viajesDia: ViajesDia[]) {
    let textData = '';
    viajesDia.forEach(viajeDia => {
      textData += `Case #${viajeDia.dia}: ${viajeDia.cantidadViajes}\n`;
    });
    this.utilService.createAndDownLoadFile(textData, `${this.mudanzaData.cedulaParticipante}`);
  }


  convertToNumber(text: any): number {
    const num: number = Number(text);
    if ( !num ) {
      throw new Error(this.translate.instant('MESSAGES.ERROR_DATA_TYPE'));
    }
    return num;
  }

  validarDiasTrabajo(t: any): number {
    t = this.convertToNumber(t);

    if ( !(t >= 1 && t <= 500) ) {
      throw new Error(this.translate.instant('MESSAGES.ERROR_NUM_DIAS_TRABAJO'));
    }
    return t;
  }

  validarCantidadElementos(n: any): number {
    n = this.convertToNumber(n);

    if ( !(n >= 1 && n <= 100) ) {
      throw new Error(this.translate.instant('MESSAGES.ERROR_NUM_ELEMENTOS_CARGAR'));
    }
    return n;
  }

  validarPesoElemento(wi: any): number {
    wi = this.convertToNumber(wi);

    if ( !(wi >= 1 && wi <= 100) ) {
      throw new Error(this.translate.instant('MESSAGES.ERROR_PESO_ELEMENTO'));
    }
    return wi;
  }


  test() {}
}
