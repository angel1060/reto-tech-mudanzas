// import { HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { ApiMudanzasRepository } from './api.mudanzas.repository';
import { TestModule } from '../../../test.module';
import { MudanzasData } from 'src/app/model/mudanzas-data';
import { HttpClientTestingModule,  HttpTestingController } from '@angular/common/http/testing';

describe('ApiMudanzasRepository', () => {
  let repository: ApiMudanzasRepository;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestModule],
      providers: [ApiMudanzasRepository]
    });

    repository = TestBed.get(ApiMudanzasRepository);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('Es creado el repositorio', () => {
    expect(repository).toBeTruthy();
  });

  it('calcularMaximoViajesPorDia: debe retornar Observable<ViajesDia[]>', async () => {
    spyOn(repository.http, 'post').and.returnValue(of(true));

    repository.calcularMaximoViajesPorDia(new MudanzasData()).subscribe(res => {
      expect(res).toBeTruthy();
    });
  });

  // it('getItemUser: should return an Observable<[]> with response data', async () => {
  //   spyOn(service.http, 'get').and.returnValue(of(true));
  //   service.getItemUser().subscribe(res => {
  //     expect(res).toBeTruthy();
  //   });
  // });

  // it('getItemId: should return an Observable<[]> with response data', async () => {
  //   spyOn(service.http, 'get').and.returnValue(of(true));
  //   service.getItemId(rol).subscribe(res => {
  //     expect(res).toBeTruthy();
  //   });
  // });
});
