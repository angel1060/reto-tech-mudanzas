import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MudanzasRepository } from './mudanzas.repository';
import { ViajesDia } from '../../../model/viajes-dia';
import { MudanzasData } from '../../../model/mudanzas-data';
import { AppConfig } from '../../../config/app.config';

@Injectable({ providedIn: 'root' })
export class ApiMudanzasRepository implements MudanzasRepository {
  public url = `${this.appConfig.URL}/mudanzas/api/`;

  constructor(public appConfig: AppConfig, public http: HttpClient) {}

  calcularMaximoViajesPorDia(mudanzasData: MudanzasData): Observable<ViajesDia[]> {
    return this.http.post<ViajesDia[]>(this.url, mudanzasData);
  }
}
