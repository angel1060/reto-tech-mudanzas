import { Observable, of } from 'rxjs';
import { MudanzasRepository } from './mudanzas.repository';
import { MudanzasData } from '../../../model/mudanzas-data';
import { ViajesDia } from '../../../model/viajes-dia';
import { Viaje } from '../../../model/viaje';
import { Elemento } from '../../../model/elemento';

export class FakeMudanzasRepository implements MudanzasRepository {
  url: string;

  calcularMaximoViajesPorDia(mudanzasData: MudanzasData): Observable<ViajesDia[]> {
    return of(this.crearViajes());
  }

  crearViajes() {
    const viajes = [];

    viajes.push(
      new ViajesDia(1, 1, [
        new Viaje([
          new Elemento(0, 30),
          new Elemento(1, 30),
          new Elemento(2, 1),
          new Elemento(3, 1)
        ])
      ]
    ));

    return viajes;
  }
}
