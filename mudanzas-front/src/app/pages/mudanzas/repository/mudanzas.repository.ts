import { Observable } from 'rxjs';
import { ViajesDia } from '../../../model/viajes-dia';
import { MudanzasData } from '../../../model/mudanzas-data';

export const MUDANZAS_REPOSITORY = 'MudanzasRepository';

export interface MudanzasRepository {
  url: string;
  calcularMaximoViajesPorDia(mudanzasData: MudanzasData): Observable<ViajesDia[]>;
}
