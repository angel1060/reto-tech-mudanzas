import { HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MudanzasService } from './mudanzas.service';
import { TestModule } from '../test.module';
import { MudanzasData } from '../model/mudanzas-data';
import { FakeMudanzasRepository } from '../pages/mudanzas/repository/fake.mudanzas.repository';
import { Observable } from 'rxjs';

describe('MudanzasService', () => {
  let service: MudanzasService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestModule],
      providers: [MudanzasService]
    });

    service = TestBed.get(MudanzasService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('Es creado el servicio', () => {
    expect(service).toBeTruthy();
  });

  it('calcularMaximoViajesPorDia: debe retornar Observable<ViajesDia[]>', async () => {
    spyOn( service.repository, 'calcularMaximoViajesPorDia' ).and.returnValue( Observable.create() );
    service.calcularMaximoViajesPorDia(new MudanzasData()).subscribe(res => {
      expect(Array.isArray(res)).toBeTruthy();
    });
  });
});
