import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { MudanzasData } from '../model/mudanzas-data';
import { ViajesDia } from '../model/viajes-dia';
import { MUDANZAS_REPOSITORY, MudanzasRepository } from '../pages/mudanzas/repository/mudanzas.repository';

@Injectable({
  providedIn: 'root'
})
export class MudanzasService {
  constructor(
    @Inject(MUDANZAS_REPOSITORY) public repository: MudanzasRepository
  ) {}

  calcularMaximoViajesPorDia(mudanzasData: MudanzasData): Observable<ViajesDia[]> {
    return this.repository.calcularMaximoViajesPorDia(mudanzasData);
  }
}
