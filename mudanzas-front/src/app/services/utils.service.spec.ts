import { HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MudanzasService } from './mudanzas.service';
import { TestModule } from '../test.module';
import { MudanzasData } from '../model/mudanzas-data';
import { FakeMudanzasRepository } from '../pages/mudanzas/repository/fake.mudanzas.repository';
import { Observable } from 'rxjs';
import { UtilsService } from './utils.service';

describe('UtilsService', () => {
  let service: UtilsService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TestModule],
      providers: [UtilsService]
    });

    service = TestBed.get(UtilsService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('Es creado el servicio', () => {
    expect(service).toBeTruthy();
  });

  it('Reemplaza variables en cadena string', async () => {
    const response = service.replaceStringValues('#0#', ['Hola']);
    expect( response ).toBe('Hola');
  });

});
