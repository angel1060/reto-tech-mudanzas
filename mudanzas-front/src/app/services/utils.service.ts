import { Injectable, ViewChild } from '@angular/core';
import { AppConfig } from '../config/app.config';
import { saveAs } from 'file-saver';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(
    public appConfig: AppConfig
  ) {}

  createAndDownLoadFile(data: any, fileName: string, extension?: string) {
      const blob = new Blob([data], {type: this.appConfig.TEXT_PLAIN});
      saveAs(blob, `${fileName}.${extension ? extension : this.appConfig.FILE_PLAIN_EXTENSION}`);
  }

  errorMessage(mesage: string) {
    Swal.fire({
      title: '',
      text: mesage,
      type: 'error'
    });
  }

  replaceStringValues(str: string, values: any[]): string {
    values.forEach((item, index) => {
      str = str.replace(`#${index}#`, item);
    });

    return str;
  }


}
