import { ModuleWithProviders, NgModule } from '@angular/core';
import { ApiMudanzasRepository } from '../pages/mudanzas/repository/api.mudanzas.repository';

@NgModule()
export class UtilsModule {
  public static forRoot(): ModuleWithProviders {
    return {
      ngModule: UtilsModule,
      providers: [
        { provide: 'MudanzasRepository', useClass: ApiMudanzasRepository }
      ]
    };
  }
}
