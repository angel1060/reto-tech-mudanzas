import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService } from '@ngx-translate/core';
import { UtilsModule } from './shared/index.default';
import { AppConfig } from './config/app.config';


@NgModule({
  exports: [
    FormsModule,
    ReactiveFormsModule,
    NoopAnimationsModule,
    HttpClientTestingModule,
    RouterTestingModule,
    UtilsModule
  ],
  entryComponents: [],
  providers: [
    AppConfig,
    TranslateService
  ],
  imports: [UtilsModule.forRoot()]
})
export class TestModule {}
